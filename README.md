# 文件上传控件
绑定在input上的ajax异步图片上传控件。
仅在Chrome浏览器测试，其它浏览器暂未测试
用法：

Html：
```
<input type='file' name='image' class='imgUpload-multiple' >
```
JavaScript：
```
ImageUpload({
    uploadUrl:'your url',
    bind:'.imgUpload-multiple',
    limit:5,
    maxSize:'3mb'
});
```
服务器返回示例（多个图片）：
成功：
```
{
    state:1,
    path:'/upload/images/24234235.jpg|/upload/images/24238930.jpg',
    msg:'上传成功'
}
```
失败：
```
{
    state:0,
    errormsg:'上传失败'
}
```
支持一个页面多个上传控件